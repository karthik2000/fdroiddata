Categories:System
License:GPLv3
Web Site:https://gitlab.com/xphnx/onze_cm11_theme/blob/HEAD/README.md
Source Code:https://gitlab.com/xphnx/onze_cm11_theme
Issue Tracker:https://gitlab.com/xphnx/onze_cm11_theme/issues

Auto Name:Onze
Summary:CM11 FLOSS Theme
Description:
A port of TwelF CM12 Theme. Onze is a Material Design inspired theme for
CM11 aiming to provide a consistent and minimalistic look to your device.

Features:

* FLOSS Icon Pack
* Bootanimation
* Wallpaper & Lockscreen
* Alarm & Ringtone

For issues, comments and icon request, please use the issue tracker.

[https://gitlab.com/xphnx/onze_cm11_theme/blob/HEAD/CHANGELOG.md Changelog]
.

Repo Type:git
Repo:https://gitlab.com/xphnx/onze_cm11_theme.git

Build:0.4,4
    commit=ddf5077f179f2c1f6054cc6d15816019f3f184fd
    subdir=theme
    gradle=yes

Build:0.6,6
    commit=v0.6
    subdir=theme
    gradle=yes

Build:0.8,8
    commit=v0.8
    subdir=theme
    gradle=yes

Build:0.9,9
    commit=v0.9
    subdir=theme
    gradle=yes

Build:1.1,11
    commit=v1.1
    subdir=theme
    gradle=yes

Build:1.2,12
    commit=v1.2
    subdir=theme
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2
Current Version Code:12
