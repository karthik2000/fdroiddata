Categories:System
License:Missing
Web Site:http://sourceforge.net/projects/activitylauncher
Source Code:http://sourceforge.net/p/activitylauncher/code/ci/master/tree
Issue Tracker:http://sourceforge.net/p/activitylauncher/tickets

Auto Name:Activity Launcher
Summary:Create shortcuts for apps and activities
Description:
Create shortcuts for any installed app and even hidden activities to
launch them with ease.
.

Repo Type:git
Repo:http://git.code.sf.net/p/activitylauncher/code

Build:1.5.1,9
    disable=license missing
    commit=774334444567ae561c9b34d12a79b94ae3733101
    target=android-19
    extlibs=android/android-support-v4.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.5.1
Current Version Code:9

