Categories:Internet
License:GPLv3
Web Site:http://miku-nyan.github.io/Overchan-Android/
Source Code:https://github.com/miku-nyan/Overchan-Android
Issue Tracker:https://github.com/miku-nyan/Overchan-Android/issues
Donate:http://miku-nyan.github.io/Overchan-Android/en.html#Donate
Bitcoin:1LaumSD5Y9npHxsq9Cqo3esmWiytqv95QW

Auto Name:Overchan
Summary:Browse multiple imageboards
Description:
Mobile application for browsing imageboards. Supports 4chan, krautchan,
8chan, iichan.hk, dobrochan, 2ch (russian dvach) and some others.

Some features:

* Separate preferences for each imageboard: password for post deletion, HTTP/SSL, proxy
* Automatic hide posts and threads using regular expressions
* Automatic update all open tabs in background
* Sending posts in background
* Saving threads with all attachments in HTML with Dollchan
* Two ways to display replies to posts: as separate references and as list
* Support the tablet interface
* Themes
.

Repo Type:git
Repo:https://github.com/miku-nyan/Overchan-Android.git

Build:1.0.3,18
    commit=v1.0.3
    gradle=yes
    rm=libs/*,build.xml
    buildjni=no
    ndk=r10d

Build:1.0.4,19
    commit=v1.0.4
    gradle=yes
    rm=libs/*,build.xml
    buildjni=no
    ndk=r10d

Build:1.0.5,20
    commit=v1.0.5
    gradle=yes
    rm=libs/*,build.xml
    buildjni=no
    ndk=r10d

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0.5
Current Version Code:20

